import 'dart:io';

void main() {
  //bool active = true;
  double? totalResult;
  while(true){
    totalResult = calculateWithNullCheck(total:totalResult);
    if(totalResult!=null){
      print('Result = $totalResult');
    }else{
      print('Error');
    }
    print("Exit? (y/n/c)");
    String? rerun = stdin.readLineSync();
    if(rerun=='y'){
      //active = false;
      break;
    }else if(rerun=='c'){
      totalResult=null;
    }else if(rerun=='n'){
      //active = true;
      continue;
    }else{
      break;
    }
  }
}

double? calculate({double? result}) {
  double inputFirstNumber;
  if(result==null){
    print("Enter first number :");
    inputFirstNumber = double.parse(stdin.readLineSync()!);
  }else{
    inputFirstNumber = result;
    print('Your number is : $inputFirstNumber');
  }
  print("Enter sign :");
  String? inputSign = stdin.readLineSync()!;
  print("Enter second number :");
  double? inputSecondNumber = double.parse(stdin.readLineSync()!);
  return outputCalculation(firstNumber:inputFirstNumber, secondNumber:inputSecondNumber, sign:inputSign);
}

double? outputCalculation({required double firstNumber, required double secondNumber, required String sign}){
  if(sign=='+'){
    return add(firstNumber, secondNumber);
  }else if(sign=='-'){
    return minus(firstNumber, secondNumber);
  }else if(sign=='*'){
    return multiply(firstNumber, secondNumber);
  }else if(sign=='/'){
    if(!errorCheckDivideByZero(secondNumber)){
      return divide(firstNumber, secondNumber);
    }else{
      return null;
    }
  }else if(sign=='%'){
    return mod(firstNumber, secondNumber);
  }else{
    return null;
  }
}

double? calculateWithNullCheck({double? total}){
  if(total==null){
    return calculate();
  }else{
    return calculate(result: total);
  }
}

bool errorCheckDivideByZero(double number){
  if(number!=0){
    return false;
  }else{
    return true;
  }
}

double add(double firstNumber, double secondNumber) => firstNumber+secondNumber;

double minus(double firstNumber, double secondNumber) => firstNumber-secondNumber;

double multiply(double firstNumber, double secondNumber) => firstNumber*secondNumber;

double divide(double firstNumber, double secondNumber) => firstNumber/secondNumber;

double mod(double firstNumber, double secondNumber) => firstNumber%secondNumber;
